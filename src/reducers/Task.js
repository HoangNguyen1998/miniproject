import * as Types from "../constants/ActionTypes";
var randomstring = require("randomstring");
var data = JSON.parse(localStorage.getItem("Task"));
var initialState = data ? data : [];
var myReducer = (state = initialState, action) => {
  var index = -1;
  switch (action.type) {
    case Types.SAVE_TASK:
      var task = {
        id: randomstring.generate(10),
        taskname: action.task.taskname,
        taskdescription: action.task.taskdescription,
        position: 1
      };
      state.push(task);
      localStorage.setItem("Task", JSON.stringify(state));
      return [...state];

    case Types.MOVE_TYPE_ONE:
      index = findTask(state, action.id);
      if (action.position === 1) {
        state[index].position = 2;
      }
      if (action.position === 2) {
        state[index].position = 1;
      }
      if (action.position === 3) {
        state[index].position = 1;
      }
      localStorage.setItem("Task", JSON.stringify(state));
      return [...state];

    case Types.MOVE_TYPE_TWO:
      index = findTask(state, action.id);
      if (action.position === 1) {
        state[index].position = 3;
      }
      if (action.position === 2) {
        state[index].position = 3;
      }
      if (action.position === 3) {
        state[index].position = 2;
      }
      localStorage.setItem("Task", JSON.stringify(state));
      return [...state];

    case Types.UPDATE_TASK:
      index=findTask(state, action.task.id)
      if(action.id!==-1){
        task={
          taskname: action.task.taskname,
          taskdescription:action.task.taskdescription
        }
        state[index].taskname=task.taskname
        state[index].taskdescription=task.taskdescription
      }
      localStorage.setItem("Task", JSON.stringify(state));
      return [...state]

    case Types.DELETE_TASK:
      index=findTask(state, action.id)
      state.splice(index,1)
      localStorage.setItem("Task", JSON.stringify(state));
      return [...state]
    default:
      return state;
  }
};
var findTask = (cart, id) => {
  var index = -1;
  if (cart.length > 0) {
    for (var i = 0; i < cart.length; i++) {
      if (cart[i].id === id) {
        index = i;
        break;
      }
    }
  }
  return index;
};
export default myReducer;
