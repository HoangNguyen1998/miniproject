import { combineReducers } from "redux";
import UserDetail from "./UserDetail";
import Task from './Task'
const myReducer = combineReducers({
  UserDetail,
  Task
});

export default myReducer;
