import * as Types from "../constants/ActionTypes";
var randomstring = require("randomstring");
var data = JSON.parse(localStorage.getItem("UserDetail"));
var initialState = data ? data : [];
var myReducer = (state = initialState, action) => {
  switch (action.type) {
    case Types.SAVE_USER_DETAIL:
      var detail = {
        id: randomstring.generate(10),
        name: action.detail.name,
        dob: action.detail.dayofbirth,
        email: action.detail.email,
        age: action.detail.age,
        gender: action.detail.gender
      };
      state.push(detail);
      localStorage.setItem("UserDetail", JSON.stringify(state));
      return [...state];
    case Types.UPDATE_USER_DETAIL:
      detail = {
        id: action.detail.id,
        name: action.detail.name,
        dob: action.detail.dob,  
        email: action.detail.email,
        age: action.detail.age,
        gender: action.detail.gender
      };
      state[state.length-1] = detail;
      localStorage.setItem("UserDetail", JSON.stringify(state));
      return [...state];
    default:
      return state;
  }
};

export default myReducer;
