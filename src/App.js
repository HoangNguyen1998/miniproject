import React from "react";
import "./App.css";
import { Route, Switch } from "react-router-dom";
import UserDetailContainer from "./containers/UserDetailContainer";
import TaskAddContainer from "./containers/TaskAddContainer"
import TaskContainer from './containers/TaskContainer'
import TaskEditContainer from './containers/TaskEditContainer'
function App() {
  return (
      <div className="App">
        <div className="container mt-5">
          <Switch>
            <Route exact path="/" component={UserDetailContainer} />
            <Route exact path="/task" component={TaskContainer} />
            <Route exact path="/taskedit" component={TaskAddContainer}/>
            <Route exact path="/task/:id" component = {TaskEditContainer}/>
          </Switch>
        </div>
        </div>
  );
}

export default App;
