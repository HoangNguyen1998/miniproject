import React, { Component } from 'react';
import {Link} from 'react-router-dom'

class TaskItemEdit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      taskname: "",
      taskdescription: ""
    };
  }
  componentWillMount() {
    if(this.props.tasks){
      for(var i=0;i<this.props.tasks.length;i++){
        if(this.props.tasks[i].id===this.props.index){
          this.setState({
            id: this.props.tasks[i].id,
            taskname: this.props.tasks[i].taskname,
            taskdescription: this.props.tasks[i].taskdescription
          })
          break;
        }
      }
    }
  }
  onChange = event => {
    var target = event.target;
    var name = target.name;
    var value = target.value;
    this.setState({
      [name]: value
    });
  };
  onUpdate=()=>{
    this.props.onUpdateTask(this.state)
  }
    render() {
        return (
            
            <div className="container">
                  <h4 className="text-center">Edit Task</h4>
                <div className="">
                  <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                      <label>Task Name:</label>
                      <input
                        type="text"
                        className="form-control"
                        id="taskname"
                        name="taskname"
                        value={this.state.taskname}
                        onChange={this.onChange}
                      />
                    </div>
                    <div className="form-group">
                      <label>Task Description:</label>
                      <input
                        type="text"
                        className="form-control"
                        id="taskdescription"
                        name="taskdescription"
                        value={this.state.taskdescription}
                        onChange={this.onChange}
                      />
                    </div>
                  </form>
                </div>
                <div className="text-center">
                  <Link to='/task'
                    className="btn btn-success mr-4"
                    onClick={this.onUpdate}
                  >
                    Save
                  </Link>
                  <Link to='/task'
                    type="button"
                    className="btn btn-danger"
                  >
                    Close
                  </Link>
                </div>
              </div>
        );
    }
}

export default TaskItemEdit;