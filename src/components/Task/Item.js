import React, { Component } from "react";
import {Link} from 'react-router-dom'
class TaskItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: "",
      taskname: "",
      taskdescription: ""
    };
  }
  onChange = event => {
    var target = event.target;
    var name = target.name;
    var value = target.value;
    this.setState({
      [name]: value
    });
  };
  componentWillMount() {
    if (this.props.task) {
      this.setState({
        id: this.props.task.id,
        taskname: this.props.task.taskname,
        taskdescription: this.props.task.taskdescription
      });
    }
  }
  onUpdateTask = () => {
    this.props.onUpdateTask(this.state);
  };

  onDeleteTask=(id)=>{
    this.props.onDeleteTask(id)
  }
  onMoveOne = (id, position) => {
    this.props.onMoveOne(id, position);
  };

  onMoveTwo = (id, position) => {
    this.props.onMoveTwo(id, position);
  };

  onTest=()=>{
    this.props.history.push('/task/'+this.state.id)
  }
  render() {
    var { task } = this.props;
    var {taskname}=this.state
    return (
      <tr>
        <th scope="row">
          {task.taskname}
          <div className="btn-group pull-right">
            <button
              type="button"
              data-toggle="dropdown" 
              aria-haspopup="true"
              aria-expanded="false"
            >
              <i className="fa fa-ellipsis-v" />
            </button>
            <div className="dropdown-menu">
              <Link to={'/task/'+this.state.id}
                className="dropdown-item"
              >
                Edit
              </Link>
               {/* <button
                className="dropdown-item"
                onClick={this.onTest}
              >
                Edit
              </button> */}
              <button 
                className="dropdown-item"
                onClick={() => this.onMoveOne(task.id, task.position)}
              >
                {task.position === 1
                  ? "Move to In Process"
                  : task.position === 2
                  ? "Move to New"
                  : "Move to New"}
              </button>
              <button
                className="dropdown-item"
                onClick={() => this.onMoveTwo(task.id, task.position)}
              >
                {task.position === 1
                  ? "Move to Resolved"
                  : task.position === 2
                  ? "Move to Resolved"
                  : "Move to In Process"}
              </button>
              <button className="dropdown-item" onClick={()=>this.onDeleteTask(task.id)}>
                Delete
              </button>
            </div>
            {/* Modal */}
            <div className="modal fade" id="myModal" role="dialog">
              <div className="modal-dialog">
                {/* Modal content*/}
                <div className="modal-content">
                  <div className="modal-header">
                    <button
                      type="button"
                      className="close"
                      data-dismiss="modal"
                    >
                      ×
                    </button>
                    <h4 className="modal-title">Edit Task</h4>
                  </div>
                  <div className="modal-body">
                    <form onSubmit={this.onSubmit}>
                      <div className="form-group">
                        <label>Task Name:</label>
                        <input
                          type="text"
                          className="form-control"
                          id="taskname"
                          name="taskname"
                          value={taskname}
                          onChange={this.onChange}
                        />
                      </div>
                      <div className="form-group">
                        <label>Task Description:</label>
                        <input
                          type="text"
                          className="form-control"
                          id="taskdescription"
                          name="taskdescription"
                          value={this.state.taskdescription}
                          onChange={this.onChange}
                        />
                      </div>
                    </form>
                  </div>
                  <div className="modal-footer">
                    <button
                      className="btn btn-success"
                      data-dismiss="modal"
                      onClick={this.onUpdateTask}
                    >
                      Save
                    </button>
                    <button
                      type="button"
                      className="btn btn-danger"
                      data-dismiss="modal"
                    >
                      Close
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </th>
      </tr>
    );
  }
}

export default (TaskItem);
