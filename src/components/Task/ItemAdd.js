import React, { Component } from "react";
import {withRouter} from 'react-router'

class UserPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      taskname: "",
      taskdescription: "",
      creator: "",
    };
  }

  onChange = event => {
    var target = event.target;
    var name = target.name;
    var value = target.value;
    this.setState({
      [name]: value
    });
  };
  onSubmit = event => {
    event.preventDefault();
    this.props.onSaveTask(this.state);
    this.props.history.push('/task');
  };
  render() {
    return (
        <div>
          <form onSubmit={this.onSubmit}>
            <div className="form-group">
              <label>Task Name:</label>
              <input
                type="text"
                className="form-control"
                id="taskname"
                name="taskname"
                onChange={this.onChange}
              />
            </div>
            <div className="form-group">
              <label>Task Description:</label>
              <input
                type="text"
                className="form-control"
                id="taskdescription"  
                name="taskdescription"
                onChange={this.onChange}
              />
            </div>
            <div className="form-group">
              <label>Creator:</label>
              <input
                type="text"
                className="form-control"
                id="creator"
                name="creator"
                defaultValue={this.props.UserDetail}
              />
            </div>

            <div className="text-center">
                <button type="submit" className="btn btn-primary">
                  <span className="fa fa-plus" /> Save
                </button>
              &emsp;
              <button
                type="button"
                className="btn btn-danger"
                onClick={this.onClear}
              >
                <span className="fa fa-times-circle" /> Close
              </button>
            </div>
          </form>
        </div>
    );
  }
}

export default withRouter(UserPage);
