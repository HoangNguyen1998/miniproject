import React, { Component } from "react";
import { withRouter } from "react-router";
var array = {};
class Page extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      dob: Date,
      email: "",
      age: Number,
      gender: true
    };  
  }
  onChange = event => {
    var target = event.target;
    var name = target.name;
    var value = target.value;
    if (name === "gender") {
      value = target.value === "true" ? true : false;
    }
    this.setState({
      [name]: value
    });
  };
  componentWillMount() {
    if (this.props.UserDetail) {
      this.setState({
        name: this.props.UserDetail[this.props.UserDetail.length - 1].name,
        dob: this.props.UserDetail[this.props.UserDetail.length - 1].dob,
        email: this.props.UserDetail[this.props.UserDetail.length - 1].email,
        age: this.props.UserDetail[this.props.UserDetail.length - 1].age,
        gender: this.props.UserDetail[this.props.UserDetail.length - 1].gender
      });
    }
  }
  onSave = () => {
    this.props.onUpdateUserDetail(this.state);
  };
  onDirect = () => {
    this.props.history.push("/taskedit");
  };
  onCatchState = () => {
    array = this.state;
  };
  onReset=()=>{
    console.log(array)
    this.setState({
      name: array.name,
      dob: array.dob,
      email: array.email,
      age: array.age,
      gender: array.gender
    })
  }
  render() {
    var {
      showTaskItemNew,
      showTaskItemInProcess,
      showTaskItemResolved
    } = this.props;
    return (
      <div className="container">
        <div>
          <h1>Task List</h1>
          <hr />
        </div>
        <div className="mb-3">
          <button
            type="button"
            className="btn btn-primary mr-1"
            data-toggle="modal"
            data-target="#ModalUser"
            onClick={this.onCatchState}
          >
            Hello {this.props.UserDetail[this.props.UserDetail.length - 1].name}
          </button>
          <button
            type="button"
            className="btn btn-primary"
            onClick={this.onDirect}
          >
            <i className="fa fa-plus-circle" /> Add New Task
          </button>
        </div>
        <div className="row">
          {/* cot New */}
          <div className="col-sm-4">
            <table className="table table-bordered">
              <thead>
                <tr>
                  <th scope="col">New</th>
                </tr>
              </thead>
              <tbody>{showTaskItemNew}</tbody>
            </table>
          </div>
          {/* het cot New */}
          {/* cot In Process */}
          <div className="col-sm-4">
            <table className="table table-bordered">
              <thead>
                <tr>
                  <th scope="col">In Process</th>
                </tr>
              </thead>
              <tbody>{showTaskItemInProcess}</tbody>
            </table>
          </div>
          {/* het cot In Process */}
          {/* cot In Resolved */}
          <div className="col-sm-4">
            <table className="table table-bordered">
              <thead>
                <tr>
                  <th scope="col">Resolved</th>
                </tr>
              </thead>
              <tbody>{showTaskItemResolved}</tbody>
            </table>
          </div>
          {/* het cot Resolved */}
        </div>

        {/* Modal */}
        <div className="modal fade" id="ModalUser" role="dialog">
          <div className="modal-dialog">
            {/* Modal content*/}
            <div className="modal-content">
              <div className="modal-header">
                <h4 className="modal-title">Add user details</h4>
                <button
                  type="button"
                  className="close pull-left"
                  data-dismiss="modal"
                >
                  ×
                </button>
              </div>
              <div className="modal-body">
                <form onSubmit={this.onSubmit}>
                  <div className="form-group">
                    <label>Name:</label>
                    <input
                      type="text"
                      className="form-control"
                      id="name"
                      name="name"
                      value={this.state.name}
                      onChange={this.onChange}
                    />
                  </div>
                  <div className="form-group">
                    <label>Day Of Birth:</label>
                    <input
                      type="date"
                      className="form-control"
                      id="dob"
                      name="dob"
                      value={this.state.dob}
                      onChange={this.onChange}
                    />
                  </div>
                  <div className="form-group">
                    <label>Email:</label>
                    <input
                      type="email"
                      className="form-control"
                      id="email"
                      name="email"
                      value={this.state.email}
                      onChange={this.onChange}
                    />
                  </div>
                  <div className="form-group">
                    <label>Age:</label>
                    <input
                      type="number"
                      className="form-control"
                      id="age"
                      name="age"
                      value={this.state.age}
                      onChange={this.onChange}
                    />
                  </div>
                  <div className="form-group">
                    <select
                      value={this.state.gender}
                      name="gender"
                      className="form-control"
                      onChange={this.onChange}
                    >
                      <option value={true}>Male</option>
                      <option value={false}>Female</option>
                    </select>
                  </div>
                  <div className="text-center">
                    <button
                      className="btn btn-success mr-4"
                      data-dismiss="modal"
                      onClick={this.onSave}
                    >
                      Save
                    </button>
                    <button type="reset" className="btn btn-danger" onClick={this.onReset}>
                      Reset
                    </button>
                  </div>
                </form>
              </div>
              <div className="modal-footer" />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(Page);
