import React, { Component } from "react";
import {Link} from "react-router-dom";

class Page extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      dob: Date,
      email: "",
      age: Number,
      gender: true
    };
  }

  onChange = event => {
    var target = event.target;
    var name = target.name;
    var value = target.value;
    if (name === "gender") {
      value = target.value === "true" ? true : false;
    }
    this.setState({
      [name]: value
    });
  };
  onSubmit = () => {
    this.props.onSaveUserDetail(this.state);
  };
  render() {
    return (
      <div>
        <h1 className="text-center">TASK APPLICATION</h1>
        <form onSubmit={this.onSubmit}>
          <div className="form-group">
            <label htmlFor="name">Name:</label>
            <input
              type="text"
              className="form-control"
              id="name"
              name="name"
              onChange={this.onChange}
            />
          </div>
          <div className="form-group">
            <label htmlFor="dob">Day Of Birth:</label>
            <input
              type="date"
              className="form-control"
              id="dayofbirth"
              name="dayofbirth"
              onChange={this.onChange}
            />
          </div>
          <div className="form-group">
            <label htmlFor="email">Email:</label>
            <input
              type="email"
              className="form-control"
              id="email"
              name="email"
              onChange={this.onChange}
            />
          </div>
          <div className="form-group">
            <label htmlFor="age">Age:</label>
            <input
              type="number"
              className="form-control"
              id="age"
              name="age"
              onChange={this.onChange}
            />
          </div>

          <div className="form-group">
            <label>Gender: </label>
            <select
              name="gender"
              className="form-control mb-5"
              value={this.state.status}
              onChange={this.onChange}
            >
              <option value={true}>Male</option>
              <option value={false}>Female</option>
            </select>
          </div>

          <div className="text-center">
              <Link className="btn btn-primary" to='/task' onClick={this.onSubmit}>
                <span className="fa fa-plus"/> Save
              </Link>
          </div>
        </form>
      </div>
    );
  }
}

export default (Page);
