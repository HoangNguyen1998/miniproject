import React, { Component } from "react";
import { connect } from "react-redux";
import * as Actions from "../actions/index";
import TaskItemEdit from "../components/Task/ItemEdit";
import PropTypes from 'prop-types'

class TaskEditContainer extends Component {
  render() {
    var { onUpdateTask, tasks } = this.props;
    return <TaskItemEdit onUpdateTask={onUpdateTask} tasks={tasks} index={this.props.match.params.id}/>;
  }
}

const mapStateToProps = state => {
  return {
      tasks: state.Task
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    onUpdateTask: task => {
      dispatch(Actions.ActionUpdateTask(task));
    }
  };
};

TaskEditContainer.propTypes={
  tasks: PropTypes.arrayOf(
    PropTypes.shape({
      taskname: PropTypes.string.isRequired,
      taskdescription: PropTypes.string.isRequired
    })
  ).isRequired
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TaskEditContainer);
