import React, { Component } from "react";
import { connect } from "react-redux";
import * as Actions from "../actions/index";
import UserPage from "../components/User/Page";

class UserDetailContainer extends Component {
  render() {
    var { onSaveUserDetail } = this.props;
    // console.log("xem giá trị: ", typeof(this.props.detail[0].gender))
    return <UserPage onSaveUserDetail={onSaveUserDetail} />;
  }
}

const mapStateToProps = state => {
  return {
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    onSaveUserDetail: message => {
      dispatch(Actions.ActionSaveUserDetail(message));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserDetailContainer);
