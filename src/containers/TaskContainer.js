import React, { Component } from "react";
import { connect } from "react-redux";
import * as Actions from "../actions/index";
import TaskPage from "../components/Task/Page";
import TaskItem from "../components/Task/Item";
import PropTypes from "prop-types";

class TaskContainer extends Component {
  showTaskItemNew = tasks => {
    var { onMoveOne, onMoveTwo, onUpdateTask, onDeleteTask } = this.props;
    var elements1 = tasks.map((task, index) => {
      if (task.position === 1) {
        return (
          <TaskItem
            task={task}
            key={index}
            index={index}
            onMoveOne={onMoveOne}
            onMoveTwo={onMoveTwo}
            onUpdateTask={onUpdateTask}
            onDeleteTask={onDeleteTask}
          />
        );
      }
      return [];
    });
    return elements1;
  };
  showTaskItemInProcess = tasks => {
    var { onMoveOne, onMoveTwo, onUpdateTask, onDeleteTask } = this.props;
    var elements2 = tasks.map((task, index) => {
      if (task.position === 2) {
        return (
          <TaskItem
            task={task}
            key={index}
            onMoveOne={onMoveOne}
            onMoveTwo={onMoveTwo}
            onUpdateTask={onUpdateTask}
            onDeleteTask={onDeleteTask}
          />
        );
      }
      return [];
    });
    return elements2;
  };
  showTaskItemResolved = tasks => {
    var { onMoveOne, onMoveTwo, onUpdateTask, onDeleteTask } = this.props;
    var elements3 = tasks.map((task, index) => {
      if (task.position === 3) {
        return (
          <TaskItem
            task={task}
            key={index}
            onMoveOne={onMoveOne}
            onMoveTwo={onMoveTwo}
            onUpdateTask={onUpdateTask}
            onDeleteTask={onDeleteTask}
          />
        );
      }
      return [];
    });
    return elements3;
  };
  render() {
    var { tasks, UserDetail, onUpdateUserDetail } = this.props;
    return (
      <TaskPage
        showTaskItemNew={this.showTaskItemNew(tasks)}
        onUpdateUserDetail={onUpdateUserDetail}
        showTaskItemInProcess={this.showTaskItemInProcess(tasks)}
        showTaskItemResolved={this.showTaskItemResolved(tasks)}
        UserDetail={UserDetail}
      />
    );
  }
}

const mapStateToProps = state => {
  return {
    tasks: state.Task,
    UserDetail: state.UserDetail
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    onSaveTask: task => {
      dispatch(Actions.ActionSaveTask(task));
    },
    onMoveOne: (id, position) => {
      dispatch(Actions.ActionMoveOne(id, position));
    },
    onMoveTwo: (id, position) => {
      dispatch(Actions.ActionMoveTwo(id, position));
    },
    onUpdateUserDetail: message => {
      dispatch(Actions.ActionUpdateUserDetail(message));
    },
    onUpdateTask: task => {
      dispatch(Actions.ActionUpdateTask(task));
    },
    onDeleteTask: id => {
      dispatch(Actions.ActionDeleteTask(id));
    }
  };
};

TaskContainer.propTypes = {
  tasks: PropTypes.arrayOf(
    PropTypes.shape({
      taskname: PropTypes.string.isRequired,
      taskdescription: PropTypes.string.isRequired
    })
  ).isRequired,
  UserDetail: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired
    })
  ).isRequired
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TaskContainer);
