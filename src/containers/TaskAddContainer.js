import React, { Component } from "react";
import { connect } from "react-redux";
import * as Actions from "../actions/index";
import TaskItemAdd from "../components/Task/ItemAdd";
import PropTypes from 'prop-types'

class TaskAddContainer extends Component {
  render() {
    var { onSaveTask, UserDetail } = this.props;
    return <TaskItemAdd onSaveTask={onSaveTask} UserDetail={UserDetail}/>;
  }
}

const mapStateToProps = state => {
  return {
      UserDetail: state.UserDetail[state.UserDetail.length-1].name
  };
};

const mapDispatchToProps = (dispatch, props) => {
  return {
    onSaveTask: task => {
      dispatch(Actions.ActionSaveTask(task));
    }
  };
};

TaskAddContainer.propTypes = {
  UserDetail: PropTypes.string.isRequired
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TaskAddContainer);
