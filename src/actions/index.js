import * as ActionTypes from '../constants/ActionTypes'

export const ActionSaveUserDetail=detail=>{
    return{
        type: ActionTypes.SAVE_USER_DETAIL,
        detail
    }
}

export const ActionSaveTask=task=>{
    return{
        type: ActionTypes.SAVE_TASK,
        task
    }
}

export const ActionMoveOne=(id, position)=>{
    return{
        type: ActionTypes.MOVE_TYPE_ONE,
        id, position
    }
}

export const ActionMoveTwo=(id, position)=>{
    return{
        type: ActionTypes.MOVE_TYPE_TWO,
        id, position
    }
}

export const ActionUpdateUserDetail=detail=>{
    return{
        type: ActionTypes.UPDATE_USER_DETAIL,
        detail
    }
}

export const ActionUpdateTask=task=>{
    return{
        type: ActionTypes.UPDATE_TASK,
        task
    }
}

export const ActionDeleteTask=id=>{
    return{
        type: ActionTypes.DELETE_TASK,
        id
    }
}